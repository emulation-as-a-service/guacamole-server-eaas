#ifndef __GUAC_SDLONP_PROTOCOL_TYPES_H
#define __GUAC_SDLONP_PROTOCOL_TYPES_H

#include <stdint.h>


typedef int8_t sdlonp_opcode;

enum sdlonp_opcodes
{
	SDLONP_OPCODE_INVALID = -1,

	__SDLONP_OPCODE_OUTPUT_FIRST,

	/* Emulator's output */
	SDLONP_OPCODE_PIXEL_FORMAT = __SDLONP_OPCODE_OUTPUT_FIRST,
	SDLONP_OPCODE_SCREEN_SIZE,
	SDLONP_OPCODE_SCREEN_UPDATE,
	SDLONP_OPCODE_AUDIO_FORMAT,
	SDLONP_OPCODE_AUDIO,

	__SDLONP_OPCODE_OUTPUT_LAST,

	/* Emulator's input */
	SDLONP_OPCODE_MOUSE = __SDLONP_OPCODE_OUTPUT_LAST,
	SDLONP_OPCODE_KEY,
	SDLONP_OPCODE_DISCONNECT,

	SDLONP_OPCODE_COUNT
};


#pragma pack(push)  /* Set structure-alignment to 1-byte */
#pragma pack(1)

struct sdlonp_mouse_event
{
	int xpos;
	int ypos;
	int buttons;
};


struct sdlonp_key_event
{
	int keysym;
	int pressed;
};


struct sdlonp_pixel_format
{
	uint8_t bits_per_pixel;
	uint8_t bytes_per_pixel;
	uint8_t red_shift;
	uint8_t green_shift;
	uint8_t blue_shift;
	uint8_t alpha_shift;
	uint32_t red_mask;
	uint32_t green_mask;
	uint32_t blue_mask;
	uint32_t alpha_mask;
};


struct sdlonp_screen_size
{
	uint16_t width;
	uint16_t height;
};


struct sdlonp_screen_update_header
{
	uint16_t xpos;
	uint16_t ypos;
	uint16_t width;
	uint16_t height;
	uint32_t buffer_length;
};


struct sdlonp_audio_format
{
	uint16_t audio_rate;
	uint8_t channels;
	uint8_t bits_per_sample;
};


struct sdlonp_audio_header
{
	uint32_t buffer_length;
};

#pragma pack(pop)  /* Restore prev. structure-alignment */

typedef struct sdlonp_mouse_event sdlonp_mouse_event;
typedef struct sdlonp_key_event sdlonp_key_event;
typedef struct sdlonp_pixel_format sdlonp_pixel_format;
typedef struct sdlonp_screen_size sdlonp_screen_size;
typedef struct sdlonp_screen_update_header sdlonp_screen_update_header;
typedef struct sdlonp_audio_format sdlonp_audio_format;
typedef struct sdlonp_audio_header sdlonp_audio_header;

#endif /* __GUAC_SDLONP_PROTOCOL_TYPES_H */
