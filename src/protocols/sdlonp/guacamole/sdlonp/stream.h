#ifndef __GUAC_SDLONP_STREAM_H
#define __GUAC_SDLONP_STREAM_H

enum sdlonp_istream_mode
{
	SDLONP_ISTREAM_MODE_BLOCKING,
	SDLONP_ISTREAM_MODE_NONBLOCKING,
};


struct sdlonp_istream
{
	char* buffer;
	int position;
	int remaining;
	int capacity;
	int fd;
};


typedef struct sdlonp_istream sdlonp_istream;
typedef enum sdlonp_istream_mode sdlonp_istream_mode;


sdlonp_istream* sdlonp_istream_construct(int capacity);
void sdlonp_istream_free(sdlonp_istream* stream);

char* sdlonp_istream_read(sdlonp_istream* stream, int size, sdlonp_istream_mode mode);

inline void sdlonp_istream_enable(sdlonp_istream* stream, int fd)
{
	stream->fd = fd;
}

inline void sdlonp_istream_disable(sdlonp_istream* stream)
{
	stream->fd = -1;
}

inline int sdlonp_istream_is_enabled(sdlonp_istream* stream)
{
	return (stream->fd > 0) ? 1 : 0;
}

#endif /* __GUAC_SDLONP_STREAM_H */
