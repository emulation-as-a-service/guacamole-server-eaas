#ifndef __GUAC_SDLONP_PROTOCOL_H
#define __GUAC_SDLONP_PROTOCOL_H

#include "guacamole/sdlonp/stream.h"
#include "guacamole/sdlonp/protocol_types.h"

#include <stddef.h>
#include <sys/uio.h>

int sdlonp_write_disconnect_event(int fd);
int sdlonp_write_mouse_event(int fd, int xpos, int ypos, int buttons);
int sdlonp_write_key_event(int fd, int keysym, int pressed);
int sdlonp_write_pixel_format(int fd, sdlonp_pixel_format* format);
int sdlonp_write_screen_size(int fd, int width, int height);
int sdlonp_write_screen_update(int fd, sdlonp_screen_update_header* header, void* pixels);
int sdlonp_writev_screen_update(int fd, sdlonp_screen_update_header* header, void* pixels, int length, int pitch);
int sdlonp_write_audio_format(int fd, sdlonp_audio_format* format);
int sdlonp_write_audio_data(int fd, sdlonp_audio_header* header, void* buffer);

extern sdlonp_opcode sdlonp_read_opcode(sdlonp_istream* stream);
extern sdlonp_mouse_event* sdlonp_read_mouse_event(sdlonp_istream* stream);
extern sdlonp_key_event* sdlonp_read_key_event(sdlonp_istream* stream);
extern sdlonp_pixel_format* sdlonp_read_pixel_format(sdlonp_istream* stream);
extern sdlonp_screen_size* sdlonp_read_screen_size(sdlonp_istream* stream);
extern int sdlonp_read_screen_update_header(sdlonp_istream* stream, sdlonp_screen_update_header* header);
extern void* sdlonp_read_screen_update_pixels(sdlonp_istream* stream, sdlonp_screen_update_header* header);
extern sdlonp_audio_format* sdlonp_read_audio_format(sdlonp_istream* stream);
extern sdlonp_audio_header* sdlonp_read_audio_header(sdlonp_istream* stream);
extern void* sdlonp_read_audio_data(sdlonp_istream* stream, sdlonp_audio_header* header);

#endif /* __GUAC_SDLONP_PROTOCOL_H */
