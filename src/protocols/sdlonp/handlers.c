#include "config.h"
#include "client.h"
#include "handlers.h"
#include "helpers.h"
#include "guac_clipboard.h"
#include "guac_surface.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iconv.h>

#include <guacamole/client.h>
#include <guacamole/timestamp.h>
#include "guacamole/sdlonp/protocol.h"


/* Pointer for internal handlers for instructions from the SDLONP-Protocol */
typedef int (*__sdlonp_opcode_handler)(guac_client* client, sdlonp_istream* stream);

/* Forward-declaration for internal opcode-handlers */
int __sdlonp_handle_pixel_format(guac_client* client, sdlonp_istream* stream);
int __sdlonp_handle_screen_size(guac_client* client, sdlonp_istream* stream);
int __sdlonp_handle_screen_update(guac_client* client, sdlonp_istream* stream);
int __sdlonp_handle_audio_format(guac_client* client, sdlonp_istream* stream);
int __sdlonp_handle_audio_data(guac_client* client, sdlonp_istream* stream);

/* Registry for all opcode-handlers */
static const __sdlonp_opcode_handler __OPCODE_HANDLERS[] = {
		__sdlonp_handle_pixel_format,
		__sdlonp_handle_screen_size,
		__sdlonp_handle_screen_update,
		__sdlonp_handle_audio_format,
		__sdlonp_handle_audio_data,
	};


/* =============== Opcode-Handler Implementation =============== */

int __sdlonp_handle_pixel_format(guac_client* client, sdlonp_istream* stream)
{
	SDLONP_DEBUG("Handle pixel-format message...");

	sdlonp_pixel_format* format = sdlonp_read_pixel_format(stream);
	if (format == NULL) {
		guac_client_log(client, GUAC_LOG_ERROR, "Emulator's pixel-format could not be read.");
		return -1;
	}

	sdlonp_guac_client_data* client_data = (sdlonp_guac_client_data*) client->data;
	const int length = sizeof(sdlonp_pixel_format);
	memcpy(&client_data->emu_pixel_format, format, length);

	return 0;
}


int __sdlonp_handle_screen_size(guac_client* client, sdlonp_istream* stream)
{
	SDLONP_DEBUG("Handle screen-size message...");

	sdlonp_screen_size* newsize = sdlonp_read_screen_size(stream);
	if (newsize == NULL) {
		guac_client_log(client, GUAC_LOG_ERROR, "The screen-update's size could not be read.");
		return -1;
	}

	sdlonp_guac_client_data* client_data = (sdlonp_guac_client_data*) client->data;
	if (client_data->default_surface != NULL) {
		/* Surface already exists, resize it */
		guac_common_surface_resize(client_data->default_surface, newsize->width, newsize->height);
	}
	else {
		/* Allocate a new surface */
		client_data->default_surface = guac_common_surface_alloc(client->socket,
				GUAC_DEFAULT_LAYER, newsize->width, newsize->height);

		/* Fill the new surface with black */
		cairo_surface_t* surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, newsize->width, newsize->height);
		cairo_t* context = cairo_create(surface);
		cairo_set_source_rgb(context, 0.0, 0.0, 0.0);
		cairo_rectangle(context, 0.0, 0.0, (double) newsize->width, (double) newsize->height);
		cairo_fill(context);

		/* Send updated surface to client */
		guac_common_surface_draw(client_data->default_surface, 0, 0, surface);

		cairo_destroy(context);
		cairo_surface_destroy(surface);
	}

	return 0;
}


static inline void __sdlonp_copy_pixels_1to4(void* srcbuf, unsigned int* dstbuf, int width, int height)
{
	unsigned char* cursrc = srcbuf;
	unsigned int* curdst = dstbuf;

	int x, y;

	/* Copy 8-bit pixels to 24-bit destination */
	for (y = 0; y < height; ++y) {
		for (x = 0; x < width; ++x) {
			/* Output RGB */
			const unsigned char pixel = *cursrc;
			*curdst = (pixel << 16) | (pixel << 8) | pixel;

			/* Advance to next pixel */
			++cursrc;
			++curdst;
		}
	}
}


static inline void __sdlonp_copy_pixels_2to4(void* srcbuf, unsigned int* dstbuf, int width, int height, const sdlonp_pixel_format* format)
{
	const unsigned int red_max   = (format->red_mask   >> format->red_shift  ) + 1;
	const unsigned int green_max = (format->green_mask >> format->green_shift) + 1;
	const unsigned int blue_max  = (format->blue_mask  >> format->blue_shift ) + 1;

	const unsigned int red_scaling   = 0x100 / red_max;
	const unsigned int green_scaling = 0x100 / green_max;
	const unsigned int blue_scaling  = 0x100 / blue_max;

	unsigned char* cursrc = srcbuf;
	unsigned int* curdst = dstbuf;

	int x, y;

	/* Copy 16-bit pixels to 24-bit destination */
	for (y = 0; y < height; ++y) {
		for (x = 0; x < width; ++x) {
			/* Translate color-value to RGB */
			const unsigned short pixel = *((unsigned short*) cursrc);
			const unsigned char red   = (pixel >> format->red_shift)   * red_scaling;
			const unsigned char green = (pixel >> format->green_shift) * green_scaling;
			const unsigned char blue  = (pixel >> format->blue_shift)  * blue_scaling;

			/* Output RGB */
			*curdst = (red << 16) | (green << 8) | blue;

			/* Advance to next pixel */
			cursrc += 2;
			++curdst;
		}
	}
}


static inline void __sdlonp_copy_pixels_3to4(void* srcbuf, unsigned int* dstbuf, int width, int height)
{
	unsigned char* cursrc = srcbuf;
	unsigned int* curdst = dstbuf;

	int x, y;

	/* Copy packed 24-bit (3 byte) pixels to 24-bit (4 byte) destination */
	for (y = 0; y < height; ++y) {
		for (x = 0; x < width; ++x) {
			/* No conversion needed! */
			const unsigned char red   = *(cursrc + 2);
			const unsigned char green = *(cursrc + 1);
			const unsigned char blue  = *(cursrc + 0);

			/* Output RGB */
			*curdst = (red << 16) | (green << 8) | blue;

			/* Advance to next pixel */
			cursrc += 3;
			++curdst;
		}
	}
}


static inline void __sdlonp_copy_pixels_4to4(void* srcbuf, unsigned int* dstbuf, int width, int height)
{
	unsigned int* cursrc = srcbuf;
	unsigned int* curdst = dstbuf;

	const int imax = width * height;
	int i;

	/* Copy 32-bit pixels to 24-bit destination */
	for (i = 0; i < imax; ++i) {
		/* Output RGB only, masking out alpha value */
		const unsigned int pixel = *(cursrc + i);
		*(curdst + i) = pixel & 0xFFFFFF;
	}
}


int __sdlonp_handle_screen_update(guac_client* client, sdlonp_istream* stream)
{
	SDLONP_DEBUG("Handle screen-update message...");

	sdlonp_screen_update_header header;
	if (sdlonp_read_screen_update_header(stream, &header) != 0) {
		guac_client_log(client, GUAC_LOG_ERROR, "A screen-update's header could not be read.");
		return -1;
	}

	void* srcbuf = sdlonp_read_screen_update_pixels(stream, &header);
	if (srcbuf == NULL) {
		guac_client_log(client, GUAC_LOG_ERROR, "The screen-update's pixels could not be read.");
		return -1;
	}

	const sdlonp_guac_client_data* client_data = (sdlonp_guac_client_data*) client->data;
	const sdlonp_pixel_format* pixel_format = &(client_data->emu_pixel_format);

	if (client_data->default_surface == NULL)
		return 0;

	/* Cairo image buffer with 24-bit (4 bytes) per pixel */
	const int stride = cairo_format_stride_for_width(CAIRO_FORMAT_RGB24, header.width);
	unsigned int* dstbuf = malloc(header.height * stride);

	switch (pixel_format->bytes_per_pixel)
	{
		case 1:
			__sdlonp_copy_pixels_1to4(srcbuf, dstbuf, header.width, header.height);
			break;

		case 2:
			__sdlonp_copy_pixels_2to4(srcbuf, dstbuf, header.width, header.height, pixel_format);
			break;

		case 3:
			__sdlonp_copy_pixels_3to4(srcbuf, dstbuf, header.width, header.height);
			break;

		case 4:
			__sdlonp_copy_pixels_4to4(srcbuf, dstbuf, header.width, header.height);
			break;

		default:
			guac_client_log(client, GUAC_LOG_ERROR, "Invalid pixelformat specified! Skip screen update processing.");
	}

	/* For now, only use default layer */
	cairo_surface_t* surface = cairo_image_surface_create_for_data((unsigned char*) dstbuf, CAIRO_FORMAT_RGB24, header.width, header.height, stride);
	guac_common_surface_draw(client_data->default_surface, header.xpos, header.ypos, surface);

	/* Free surface */
	cairo_surface_destroy(surface);
	free(dstbuf);

	return 0;
}


int __sdlonp_handle_audio_format(guac_client* client, sdlonp_istream* stream)
{
	SDLONP_DEBUG("Handle audio-format message...");

	sdlonp_audio_format* format = sdlonp_read_audio_format(stream);
	if (format == NULL) {
		guac_client_log(client, GUAC_LOG_ERROR, "Emulator's audio-format could not be read.");
		return -1;
	}

	sdlonp_guac_client_data* cdata = (sdlonp_guac_client_data*) client->data;
	cdata->audio_rate = format->audio_rate;
	cdata->audio_channels = format->channels;
	cdata->audio_bps = format->bits_per_sample;

	guac_client_log(client, GUAC_LOG_INFO, "Audio format: %hu Hz, %hhu bits, %hhu channel(s)",
			format->audio_rate, format->bits_per_sample, format->channels);

	return 0;
}


int __sdlonp_handle_audio_data(guac_client* client, sdlonp_istream* stream)
{
	SDLONP_DEBUG("Handle audio message...");

	sdlonp_audio_header header;
	{
		sdlonp_audio_header* tmpheader = sdlonp_read_audio_header(stream);
		if (tmpheader == NULL) {
			guac_client_log(client, GUAC_LOG_ERROR, "Emulator's audio-header could not be read.");
			return -1;
		}

		header.buffer_length = tmpheader->buffer_length;
	}

	void* buffer = sdlonp_read_audio_data(stream, &header);
	if (buffer == NULL) {
		guac_client_log(client, GUAC_LOG_ERROR, "Emulator's audio-data could not be read.");
		return -1;
	}

	/* Stream audio-data to client */
	sdlonp_guac_client_data* cdata = (sdlonp_guac_client_data*) client->data;
	if (cdata->audio_enabled) {
		guac_audio_stream_begin(cdata->audio, cdata->audio_rate, cdata->audio_channels, cdata->audio_bps);
		guac_audio_stream_write_pcm(cdata->audio, buffer, header.buffer_length);
		guac_audio_stream_end(cdata->audio);
	}

	return 0;
}


inline void __sdlonp_surface_flush_checked(guac_common_surface* surface)
{
	if (surface != NULL)
		guac_common_surface_flush(surface);
}


/* =============== Guacamole's Handler Implementation =============== */

int sdlonp_guac_client_handle_messages(guac_client* client)
{
	sdlonp_guac_client_data* client_data = (sdlonp_guac_client_data*) client->data;
	sdlonp_istream* emu_output = client_data->emu_output;
	sdlonp_opcode opcode = SDLONP_OPCODE_INVALID;

	const int timeout = 2000;  /* 2 sec */
	const guac_timestamp max_duration = (guac_timestamp) timeout;
	const guac_timestamp endts = guac_timestamp_current() + max_duration;

	do {

		int msgcount = 0;

		/* Try to read all available messages */
		while ((opcode = sdlonp_read_opcode(emu_output)) != SDLONP_OPCODE_INVALID) {

			SDLONP_DEBUGF("Read opcode %i from input-stream %zi...", opcode, (size_t) emu_output);

			/* Check validity of received opcode */
			if (opcode < __SDLONP_OPCODE_OUTPUT_FIRST || opcode >= __SDLONP_OPCODE_OUTPUT_LAST) {
				guac_client_log(client, GUAC_LOG_ERROR, "Invalid SDLONP-Opcode received: '%i'", opcode);
				return 1;
			}

			/* Execute the opcode-handler */
			const __sdlonp_opcode_handler handler = __OPCODE_HANDLERS[opcode];
			const int retcode = (*handler)(client, emu_output);
			if (retcode != 0) {
				guac_client_log(client, GUAC_LOG_ERROR, "Handler for opcode '%i' failed.", opcode);
				return 1;
			}

			++msgcount;
		}

		/* To minimize latency, we flush message-buffers
		 * as soon as at least one message was received! */
		if (msgcount > 0)
			break;

		/* Wait for new data to become available */
		sdlonp_poll_input(emu_output->fd, timeout);

	} while (guac_timestamp_current() < endts);

	/* Send optimized screen-updates to client now */
	__sdlonp_surface_flush_checked(client_data->default_surface);

	return 0;
}


int sdlonp_guac_client_mouse_handler(guac_client* client, int x, int y, int mask)
{
	SDLONP_DEBUG("Handle mouse-message...");

	sdlonp_guac_client_data* cdata = (sdlonp_guac_client_data*) client->data;
	return sdlonp_write_mouse_event(cdata->emu_sockfd, x, y, mask);
}


int sdlonp_guac_client_key_handler(guac_client* client, int keysym, int pressed)
{
	SDLONP_DEBUG("Handle key-message...");

	sdlonp_guac_client_data* cdata = (sdlonp_guac_client_data*) client->data;
	return sdlonp_write_key_event(cdata->emu_sockfd, keysym, pressed);
}


int sdlonp_guac_client_free_handler(guac_client* client)
{
	sdlonp_guac_client_data* client_data = (sdlonp_guac_client_data*) client->data;

	/* Signal to the emulator, that the client disconnected */
	sdlonp_write_disconnect_event(client_data->emu_sockfd);

	/* Free allocated stuff */

	if (client_data->audio)
		guac_audio_stream_free(client_data->audio);

	if (client_data->default_surface)
		guac_common_surface_free(client_data->default_surface);

	sdlonp_istream_free(client_data->emu_output);

	/* Close the file-descriptors */

	if (client_data->emu_sockfd > -1)
		close(client_data->emu_sockfd);

	guac_client_log(client, GUAC_LOG_INFO, "Client's clean-up done.");

	free(client_data);
	client->data = NULL;

	return 0;
}
