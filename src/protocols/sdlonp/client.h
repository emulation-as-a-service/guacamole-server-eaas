
#ifndef __GUAC_SDLONP_CLIENT_H
#define __GUAC_SDLONP_CLIENT_H

#include "config.h"
#include "guac_clipboard.h"
#include "guac_surface.h"

#include <guacamole/sdlonp/stream.h>
#include <guacamole/sdlonp/protocol_types.h>
#include <guacamole/audio.h>
#include <guacamole/client.h>

extern char* __GUAC_CLIENT;

/** SDLONP-specific client data. */
typedef struct sdlonp_guac_client_data
{
	/** Audio output, if any. */
	guac_audio_stream* audio;

	/** Default surface. */
	guac_common_surface* default_surface;

	sdlonp_pixel_format emu_pixel_format;
	sdlonp_istream* emu_output;

	int emu_screen_width;
	int emu_screen_height;

	int emu_sockfd;

	int audio_enabled;

	/* Audio format */
	int audio_rate;
	int audio_channels;
	int audio_bps;

} sdlonp_guac_client_data;

#endif /* __GUAC_SDLONP_CLIENT_H */
