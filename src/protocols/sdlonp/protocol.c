#include "guacamole/sdlonp/protocol.h"
#include "helpers.h"

#include <unistd.h>
#include <string.h>
#include <errno.h>


#define SDLONP_DECL_MESSAGE(name)  \
		struct __##name  \
		{  \
			sdlonp_opcode opcode;  \
			name payload;  \
		};

#define SDLONP_INIT_MESSAGE(type, name, msg_opcode)  \
		struct __##type name;  \
		name.opcode = msg_opcode;

#define SDLONP_READ_DATA(type, stream, mode)  \
		(type*) sdlonp_istream_read(stream, sizeof(type), mode)

#pragma pack(push)  /* Set structure-alignment to 1-byte */
#pragma pack(1)

SDLONP_DECL_MESSAGE(sdlonp_mouse_event)
SDLONP_DECL_MESSAGE(sdlonp_key_event)
SDLONP_DECL_MESSAGE(sdlonp_screen_size)

#pragma pack(pop)  /* Restore prev. structure-alignment */


int __sdlonp_write_message(int fd, const void* message, size_t msgsize)
{
	const uint8_t* msgptr = (const uint8_t*) message;

	while (msgsize > 0) {
		const int numbytes = write(fd, msgptr, msgsize);
		if (numbytes < 0) {
			if (errno != EAGAIN && errno != EINTR) {
				SDLONP_LOGF("Could not write %zu bytes: %s", msgsize, strerror(errno));
				break;
			}

			if (errno == EINTR)
				SDLONP_LOG("Write call was interrupted! Retry...");

			SDLONP_DEBUG("Write would block, timeout for 1s");

			/* Wait and retry */
			const int timeout = 1000;  /* 1 sec */
			sdlonp_poll_output(fd, timeout);
			continue;
		}

		msgptr += numbytes;
		msgsize -= numbytes;
	}

	return (msgsize > 0);
}


int __sdlonp_writev_message(int fd, struct iovec* message, int iovcnt)
{
	for (;;) {

		int numbytes = writev(fd, message, iovcnt);
		if (numbytes < 0) {
			/* Something gone really wrong? */
			if (errno != EAGAIN && errno != EINTR) {
				SDLONP_LOGF("Could not write a multipart message! %s.", strerror(errno));
				break;  /* Yes */
			}

			/* No, retry after a timeout */
			const int timeout = 1000;  /* 1 sec */
			sdlonp_poll_output(fd, timeout);
			continue;
		}

		/* Check, if all parts were written completely */
		while (numbytes >= message->iov_len) {
			numbytes -= message->iov_len;
			++message;
			--iovcnt;

			/* Write is complete? */
			if (iovcnt == 0)
				return 0;  /* Yes */
		}

		/* No, update the pointers and retry */
		message->iov_base = ((char*) message->iov_base) + numbytes;
		message->iov_len -= numbytes;
	}

	return 1;
}


int sdlonp_write_mouse_event(int fd, int xpos, int ypos, int buttons)
{
	SDLONP_INIT_MESSAGE(sdlonp_mouse_event, message, SDLONP_OPCODE_MOUSE);
	message.payload.xpos = xpos;
	message.payload.ypos = ypos;
	message.payload.buttons = buttons;

	return __sdlonp_write_message(fd, &message, sizeof(message));
}


int sdlonp_write_key_event(int fd, int keysym, int pressed)
{
	SDLONP_INIT_MESSAGE(sdlonp_key_event, message, SDLONP_OPCODE_KEY);
	message.payload.keysym = keysym;
	message.payload.pressed = pressed;

	return __sdlonp_write_message(fd, &message, sizeof(message));
}


int sdlonp_write_disconnect_event(int fd)
{
	const sdlonp_opcode opcode = SDLONP_OPCODE_DISCONNECT;
	return __sdlonp_write_message(fd, &opcode, sizeof(sdlonp_opcode));
}


int sdlonp_write_pixel_format(int fd, sdlonp_pixel_format* format)
{
	sdlonp_opcode opcode = SDLONP_OPCODE_PIXEL_FORMAT;

	struct iovec message[2];
	message[0].iov_base = &opcode;
	message[0].iov_len = sizeof(sdlonp_opcode);
	message[1].iov_base = format;
	message[1].iov_len = sizeof(sdlonp_pixel_format);

	return __sdlonp_writev_message(fd, message, 2);
}


int sdlonp_write_screen_size(int fd, int width, int height)
{
	SDLONP_INIT_MESSAGE(sdlonp_screen_size, message, SDLONP_OPCODE_SCREEN_SIZE);
	message.payload.width = (uint16_t) width;
	message.payload.height = (uint16_t) height;

	return __sdlonp_write_message(fd, &message, sizeof(message));
}


int sdlonp_write_screen_update(int fd, sdlonp_screen_update_header* header, void* pixels)
{
	sdlonp_opcode opcode = SDLONP_OPCODE_SCREEN_UPDATE;

	struct iovec message[3];
	message[0].iov_base = &opcode;
	message[0].iov_len = sizeof(sdlonp_opcode);
	message[1].iov_base = header;
	message[1].iov_len = sizeof(sdlonp_screen_update_header);
	message[2].iov_base = pixels;
	message[2].iov_len = header->buffer_length;

	return __sdlonp_writev_message(fd, message, 3);
}


int sdlonp_writev_screen_update(int fd, sdlonp_screen_update_header* header, void* pixels, int length, int pitch)
{
	static const int MAX_IOVEC_COUNT = 128;
	static const int INITIAL_IOVEC_OFFSET = 2;
	static const int INITIAL_SCANLINE_COUNT = MAX_IOVEC_COUNT - INITIAL_IOVEC_OFFSET;

	sdlonp_opcode opcode = SDLONP_OPCODE_SCREEN_UPDATE;

	struct iovec message[MAX_IOVEC_COUNT];
	message[0].iov_base = &opcode;
	message[0].iov_len = sizeof(sdlonp_opcode);
	message[1].iov_base = header;
	message[1].iov_len = sizeof(sdlonp_screen_update_header);

	uint8_t* scanline = (uint8_t*) pixels;
	int remaining = header->height;
	int iovcnt = 0;
	int result = 0;

	/* Write the first scanlines */
	{
		if (remaining <= INITIAL_SCANLINE_COUNT) {
			/* Single call will be enough */
			iovcnt = remaining + INITIAL_IOVEC_OFFSET;
			remaining = 0;
		}
		else {
			/* Scanlines must be written using multiple calls */
			iovcnt = MAX_IOVEC_COUNT;
			remaining -= INITIAL_SCANLINE_COUNT;
		}

		/* Prepare the message's parts */
		for (int i = INITIAL_IOVEC_OFFSET; i < iovcnt; ++i) {
			message[i].iov_base = scanline;
			message[i].iov_len = length;
			scanline += pitch;
		}

		result = __sdlonp_writev_message(fd, message, iovcnt);
	}

	/* Write remaining scanlines */
	while (remaining > 0) {
		iovcnt = (remaining < MAX_IOVEC_COUNT) ? remaining : MAX_IOVEC_COUNT;
		for (int i = 0; i < iovcnt; ++i) {
			message[i].iov_base = scanline;
			message[i].iov_len = length;
			scanline += pitch;
		}

		result |= __sdlonp_writev_message(fd, message, iovcnt);
		remaining -= iovcnt;
	}

	return result;
}


int sdlonp_write_audio_format(int fd, sdlonp_audio_format* format)
{
	sdlonp_opcode opcode = SDLONP_OPCODE_AUDIO_FORMAT;

	struct iovec message[2];
	message[0].iov_base = &opcode;
	message[0].iov_len = sizeof(sdlonp_opcode);
	message[1].iov_base = format;
	message[1].iov_len = sizeof(sdlonp_audio_format);

	return __sdlonp_writev_message(fd, message, 2);
}


int sdlonp_write_audio_data(int fd, sdlonp_audio_header* header, void* buffer)
{
	sdlonp_opcode opcode = SDLONP_OPCODE_AUDIO;

	struct iovec message[3];
	message[0].iov_base = &opcode;
	message[0].iov_len = sizeof(sdlonp_opcode);
	message[1].iov_base = header;
	message[1].iov_len = sizeof(sdlonp_audio_header);
	message[2].iov_base = buffer;
	message[2].iov_len = header->buffer_length;

	return __sdlonp_writev_message(fd, message, 3);
}


inline sdlonp_opcode sdlonp_read_opcode(sdlonp_istream* stream)
{
	char* opcode = sdlonp_istream_read(stream, sizeof(sdlonp_opcode), SDLONP_ISTREAM_MODE_NONBLOCKING);
	if (opcode == NULL)
		return SDLONP_OPCODE_INVALID;

	return *((sdlonp_opcode*) opcode);
}


inline sdlonp_mouse_event* sdlonp_read_mouse_event(sdlonp_istream* stream)
{
	return SDLONP_READ_DATA(sdlonp_mouse_event, stream, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline sdlonp_key_event* sdlonp_read_key_event(sdlonp_istream* stream)
{
	return SDLONP_READ_DATA(sdlonp_key_event, stream, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline sdlonp_pixel_format* sdlonp_read_pixel_format(sdlonp_istream* stream)
{
	return SDLONP_READ_DATA(sdlonp_pixel_format, stream, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline sdlonp_screen_size* sdlonp_read_screen_size(sdlonp_istream* stream)
{
	return SDLONP_READ_DATA(sdlonp_screen_size, stream, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline int sdlonp_read_screen_update_header(sdlonp_istream* stream, sdlonp_screen_update_header* header)
{
	const size_t length = sizeof(sdlonp_screen_update_header);
	sdlonp_screen_update_header* buffer = (sdlonp_screen_update_header*)
			sdlonp_istream_read(stream, length, SDLONP_ISTREAM_MODE_BLOCKING);

	if (buffer == NULL)
		return 1;

	memcpy(header, buffer, length);
	return 0;
}


inline void* sdlonp_read_screen_update_pixels(sdlonp_istream* stream, sdlonp_screen_update_header* header)
{
	return sdlonp_istream_read(stream, header->buffer_length, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline sdlonp_audio_format* sdlonp_read_audio_format(sdlonp_istream* stream)
{
	return SDLONP_READ_DATA(sdlonp_audio_format, stream, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline sdlonp_audio_header* sdlonp_read_audio_header(sdlonp_istream* stream)
{
	return SDLONP_READ_DATA(sdlonp_audio_header, stream, SDLONP_ISTREAM_MODE_BLOCKING);
}


inline void* sdlonp_read_audio_data(sdlonp_istream* stream, sdlonp_audio_header* header)
{
	return sdlonp_istream_read(stream, header->buffer_length, SDLONP_ISTREAM_MODE_BLOCKING);
}
