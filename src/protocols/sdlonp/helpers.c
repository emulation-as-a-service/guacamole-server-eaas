#include "helpers.h"
#include <poll.h>


static inline void sdlonp_poll(int fd, short int events, int timeout)
{
	struct pollfd fds = {
			.fd = fd,
			.events = events,
			.revents = 0
	};

	poll(&fds, 1, timeout);
}


void sdlonp_poll_input(int fd, int timeout)
{
	sdlonp_poll(fd, POLLIN, timeout);
}


void sdlonp_poll_output(int fd, int timeout)
{
	sdlonp_poll(fd, POLLOUT, timeout);
}
