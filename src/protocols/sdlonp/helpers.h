#ifndef __GUAC_SDLONP_HELPERS_H
#define __GUAC_SDLONP_HELPERS_H

#include <stdio.h>
#include <syslog.h>
#include "config.h"

/* Logging Halpers */

#define SDLONP_LOG(_msg_) \
	{ printf("[SDLONP-GUAC] "_msg_"\n"); \
	  syslog(LOG_USER | LOG_INFO, "[SDLONP-GUAC] "_msg_); }

#define SDLONP_LOGF(_frmt_, ...) \
	{ printf("[SDLONP-GUAC] "_frmt_"\n", ##__VA_ARGS__); \
	  syslog(LOG_USER | LOG_INFO, "[SDLONP-GUAC] "_frmt_, ##__VA_ARGS__); }


#ifdef SDLONP_ENABLE_DEBUG_MESSAGES
#	define SDLONP_DEBUGF(_frmt_, ...)  SDLONP_LOGF(_frmt_, __VA_ARGS__)
#	define SDLONP_DEBUG(_msg_)         SDLONP_LOG(_msg_)
#else
#	define SDLONP_DEBUGF(_frmt_, ...)  { }
#	define SDLONP_DEBUG(_msg_)         { }
#endif /* SDLONP_ENABLE_DEBUG_MESSAGES */


void sdlonp_poll_input(int fd, int timeout);
void sdlonp_poll_output(int fd, int timeout);

#endif /* __GUAC_SDLONP_HELPERS_H */
