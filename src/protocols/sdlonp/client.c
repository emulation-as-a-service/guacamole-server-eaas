#include "config.h"
#include "client.h"
#include "handlers.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <guacamole/audio.h>
#include <guacamole/client.h>
#include <guacamole/protocol.h>
#include <guacamole/socket.h>
#include <guacamole/stream.h>


/* Client plugin arguments */
const char* GUAC_CLIENT_ARGS[] = {
		"emu-iosocket",
		"enable-audio",
		NULL
	};

enum SDLONP_ARGS_IDX
{
	IDX_EMU_IOSOCKET,
    IDX_ENABLE_AUDIO,
    SDLONP_ARGS_COUNT
};

char* __GUAC_CLIENT = "GUAC_CLIENT";


static int __sdlonp_file_exists(const char* filename, int num_retries, int timeout)
{
	const struct timespec interval = {
			.tv_sec  = 0,
			.tv_nsec = (long int) timeout * 1000000L
		};

	while (access(filename, F_OK) != 0) {
		/* File does not exist yet, wait and retry */
		nanosleep(&interval, NULL);
		if (--num_retries < 0)
			return 0;  /* Failure */
	}

	return 1;  /* Success */
}


static int __sdlonp_connect(guac_client* client, const char* sockname)
{
	sdlonp_guac_client_data* cdata = (sdlonp_guac_client_data*) client->data;

	const int num_retries = 2 * 15;
	const int timeout = 500;

	/* Ensure that the named-socket is created by the emulator */
	if (!__sdlonp_file_exists(sockname, num_retries, timeout)) {
		guac_client_log(client, GUAC_LOG_ERROR, "Emulator's IO-socket '%s' was not found.", sockname);
		return 0;
	}

	/* Create the socket for emulator's input/output streams */
	cdata->emu_sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (cdata->emu_sockfd < 0) {
		guac_client_log(client, GUAC_LOG_ERROR, "Creating socket for '%s' failed! %s.", sockname, strerror(errno));
		goto cleanup_on_error;
	}

	/* Setup the address of emulator's socket */
	struct sockaddr_un emuaddr;
	memset(&emuaddr, 0, sizeof(struct sockaddr_un));
	strncpy(emuaddr.sun_path, sockname, sizeof(emuaddr.sun_path) - 1);
	emuaddr.sun_family = AF_UNIX;

	/* Connect to the emulator's socket */
	const socklen_t emuaddr_length = sizeof(struct sockaddr_un);
	if (connect(cdata->emu_sockfd, (struct sockaddr*) &emuaddr, emuaddr_length) < 0) {
		guac_client_log(client, GUAC_LOG_ERROR, "Connecting to '%s' failed! %s.", sockname, strerror(errno));
		goto cleanup_on_error;
	}

	/* Connection was established successfully! */
	guac_client_log(client, GUAC_LOG_INFO, "Streaming data to/from '%s'.", sockname);
	unlink(sockname);
	return 1;

	/* Something gone wrong */
	cleanup_on_error: {

		if (cdata->emu_sockfd > 0)
			close(cdata->emu_sockfd);

		cdata->emu_sockfd = -1;
		unlink(sockname);

		return 0;
	}
}


int guac_client_init(guac_client* client, int argc, char** argv)
{
	sdlonp_guac_client_data* guac_client_data;

	/*** PARSE ARGUMENTS ***/

	if (argc != SDLONP_ARGS_COUNT) {
		guac_client_abort(client, GUAC_PROTOCOL_STATUS_SERVER_ERROR, "Wrong argument count received.");
		return 1;
	}

	const char* emu_iosocket = argv[IDX_EMU_IOSOCKET];
	if (emu_iosocket[0] == '\0') {
		guac_client_abort(client, GUAC_PROTOCOL_STATUS_SERVER_ERROR, "Emulator's IO-socket not specified.");
		return 1;
	}

	/*** INITIALIZE PLUGIN ***/

	/* Alloc client data */
	guac_client_data = malloc(sizeof(sdlonp_guac_client_data));
	memset(guac_client_data, 0, sizeof(sdlonp_guac_client_data));
	client->data = guac_client_data;

	/* If the final connect attempt fails, return error */
	if (!__sdlonp_connect(client, emu_iosocket)) {
		guac_client_abort(client, GUAC_PROTOCOL_STATUS_UPSTREAM_ERROR, "Unable to connect to the emulator.");
		free(guac_client_data);
		client->data = NULL;
		return 1;
	}

	/* Set handlers */
	client->handle_messages = sdlonp_guac_client_handle_messages;
	client->free_handler = sdlonp_guac_client_free_handler;
	client->mouse_handler = sdlonp_guac_client_mouse_handler;
	client->key_handler = sdlonp_guac_client_key_handler;

	/* Ensure connection is kept alive during lengthy connects */
	guac_socket_require_keep_alive(client->socket);

	/* Set flags */
	guac_client_data->audio_enabled = (strcmp(argv[IDX_ENABLE_AUDIO], "true") == 0);

	/* Initialize audio-system */
	if (guac_client_data->audio_enabled) {
		guac_client_data->audio = guac_audio_stream_alloc(client, NULL);
		if (guac_client_data->audio != NULL) {
			guac_client_log(client, GUAC_LOG_INFO, "Audio will be encoded as %s", guac_client_data->audio->encoder->mimetype);

			/* BUG: The stream struct is not always initialized properly!
			 *      The ack_handler must be manually set to NULL, or guacd
			 *      will sometimes crash when an ack-instruction is received. */
			guac_client_data->audio->stream->ack_handler = NULL;
		}
		else guac_client_log(client, GUAC_LOG_INFO, "No available audio encoding. Sound disabled.");
	}

	guac_client_data->default_surface = NULL;

	/* Allocate a stream-reader for emulator's output */
	guac_client_data->emu_output = sdlonp_istream_construct(256 * 1024);
	sdlonp_istream_enable(guac_client_data->emu_output, guac_client_data->emu_sockfd);

	/* Send name */
	guac_protocol_send_name(client->socket, "sdlonp-client");

	return 0;
}
