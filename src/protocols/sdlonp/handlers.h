
#ifndef __GUAC_SDLONP_HANDLERS_H
#define __GUAC_SDLONP_HANDLERS_H

#include "config.h"

#include <guacamole/client.h>


/* Regsters all handler for the SDLONP-Protocol */

int sdlonp_register_protocol_handlers(guac_client* client);


/* Handlers for Guacamole's instructions */

int sdlonp_guac_client_handle_messages(guac_client* client);
int sdlonp_guac_client_mouse_handler(guac_client* client, int x, int y, int mask);
int sdlonp_guac_client_key_handler(guac_client* client, int keysym, int pressed);
int sdlonp_guac_client_free_handler(guac_client* client);

#endif /* __GUAC_SDLONP_HANDLERS_H */
