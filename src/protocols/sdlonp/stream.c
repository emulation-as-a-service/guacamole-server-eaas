#include "guacamole/sdlonp/stream.h"
#include "helpers.h"
#include "config.h"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>


inline char* __sdlonp_istream_current_bufptr(sdlonp_istream* stream)
{
	return (stream->buffer + stream->position);
}


inline void __sdlonp_istream_advance(sdlonp_istream* stream, int size)
{
	stream->position += size;
	stream->remaining -= size;
}


sdlonp_istream* sdlonp_istream_construct(int capacity)
{
	sdlonp_istream* stream = malloc(sizeof(sdlonp_istream));
	if (stream == NULL)
		return NULL;

	stream->buffer = malloc(capacity);
	if (stream->buffer == NULL) {
		free(stream);
		return NULL;
	}

	stream->position = 0;
	stream->remaining = 0;
	stream->capacity = capacity;
	stream->fd = -1;

	return stream;
}


void sdlonp_istream_free(sdlonp_istream* stream)
{
	if (stream->buffer != NULL)
		free(stream->buffer);

	free(stream);
}


char* sdlonp_istream_read(sdlonp_istream* stream, int size, sdlonp_istream_mode mode)
{
	SDLONP_DEBUGF("Attempt to read %i bytes", size);

#ifdef DISABLE_BUFFERED_READ

	/* Unbuffered Version */

	if (stream->capacity < size) {
		free(stream->buffer);
		stream->buffer = malloc(size);
		stream->capacity = size;
	}

	char* curptr = stream->buffer;
	int numbytes_left = size;
	int numbytes_read = 0;
	do {
		const int numbytes = read(stream->fd, curptr, numbytes_left);
		if (numbytes < 0) {
			/* Read would block? */
			if ((errno == EAGAIN) && (mode == SDLONP_ISTREAM_MODE_BLOCKING)) {
				/* Yes, wait and retry */
				const static struct timespec timeout = {
						.tv_sec  = 0,
						.tv_nsec = 10L * 1000000L /* 10ms */
				};

				SDLONP_DEBUG("Read would block, timeout for 10ms");
				nanosleep(&timeout, NULL);
				continue;
			}

			if (errno != EAGAIN)
				SDLONP_LOGF("Reading %i bytes failed: %s", numbytes_left, strerror(errno));

			return NULL;  /* An error occured! */
		}
		else SDLONP_DEBUGF("Read %i of %i bytes", numbytes, size);

		curptr += numbytes;
		numbytes_read += numbytes;
		numbytes_left -= numbytes;

		/* Continue, until all of the requested data is read */

	} while (numbytes_read < size);

	return stream->buffer;

#else

	/* Buffered Version */

	char* curptr = __sdlonp_istream_current_bufptr(stream);

	/* Fast-path: Is there enough data left in the buffer? */
	if (size <= stream->remaining) {
		SDLONP_DEBUGF("Requested %i bytes available, returning", size);
		__sdlonp_istream_advance(stream, size);
		return curptr;  /* Yes */
	}

	/* Is the buffer big enough? */
	if (stream->capacity < size) {
		SDLONP_DEBUGF("Buffer not big enough, grow by %i bytes", size - stream->capacity);
		char* newbuf = malloc(size);
		memcpy(newbuf, curptr, stream->remaining);
		free(stream->buffer);
		stream->buffer = newbuf;
		stream->position = 0;
		stream->capacity = size;
	}

	/* Is enough usable space left in the buffer? */
	else if ((stream->capacity - stream->position) < size) {
		SDLONP_DEBUGF("Not enough capacity left, move %i unread bytes to beginning and refill %i bytes", stream->remaining, size - stream->remaining);
		memmove(stream->buffer, curptr, stream->remaining);
		stream->position = 0;
	}

	/* Refill the buffer with more data */
	const int offset = stream->position + stream->remaining;
	const int numbytes_target = size - stream->remaining;
	int numbytes_left = stream->capacity - offset;
	int numbytes_read = 0;
	curptr = stream->buffer + offset;
	do {
		const int flags = (mode == SDLONP_ISTREAM_MODE_NONBLOCKING) ? MSG_DONTWAIT : 0;
		const int numbytes = recv(stream->fd, curptr, numbytes_left, flags);
		if (numbytes < 1) {
			/* Read would block or is interrupted? */
			if ((errno == EINTR) || ((errno == EAGAIN) && (mode == SDLONP_ISTREAM_MODE_BLOCKING))) {
				/* Yes, wait and retry */
				if (errno == EINTR)
					SDLONP_LOG("Read was interrupted! Retry...");

				SDLONP_DEBUG("Read would block, timeout for 1s")

				const int timeout = 1000;  /* 1 sec */
				sdlonp_poll_input(stream->fd, timeout);
				continue;
			}

			if ((numbytes != 0) && (errno != EAGAIN))
				SDLONP_LOGF("Reading %i bytes failed: %s (errno %i)", size, strerror(errno), errno);

			break;  /* An error occured! */
		}
		else SDLONP_DEBUGF("Read %i bytes, required were %i bytes", numbytes, numbytes_target);

		curptr += numbytes;
		numbytes_read += numbytes;
		numbytes_left -= numbytes;

		/* Continue, until all of the requested data is read */

	} while (numbytes_read < numbytes_target);

	/* Current unread number of bytes */
	stream->remaining += numbytes_read;
	if (stream->remaining < size)
		return NULL;
	else SDLONP_DEBUGF("Requested %i bytes now available.", size);

	/* Requested amount of data was read successfully */
	curptr = __sdlonp_istream_current_bufptr(stream);
	__sdlonp_istream_advance(stream, size);
	return curptr;

#endif /* DISABLE_BUFFERED_READ */
}
